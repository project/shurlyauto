<?php
/**
 * @file
 * Settings page callback file for the ShURLy Auto module.
 */

/**
 * Menu callback;
 */
 
function shurlyauto_settings() {
  $form = array();
  
  $form['shurlyauto_settings'] = array(
    '#type' => 'fieldset',
    '#weight' => -30,
    '#title' => t('ShURLy Auto settings'),
  );
  
  $form['shurlyauto_settings']['shurly_server'] = array(
    '#type' => 'textfield',
    '#title' => t('ShURLy Server'),
    '#default_value' => variable_get('shurly_server', ''),
    '#description' => t("Enter IP or domain name, example: http://192.168.1.1, http://drupal.org, https://drupal.org"),
  );
  
  $form['shurlyauto_settings']['shurlyauto_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API Key for ShURLy Server'),
    '#default_value' => variable_get('shurlyauto_api_key', ''),
    '#description' => t("You can find API Key on the ShURLy Server. Open http://domain.tld/shurly/api/key"),
  );
  
  $node_type_array = node_type_get_types();
  foreach ($node_type_array as $k => $v) {
    $node_type_options[$k] = $v->name;
  }
  
  $form['shurlyauto_settings']['shurlyauto_node_type'] = array(
    '#title' => t('Selected node type'),
    '#type' => 'checkboxes',
    '#options' => $node_type_options,
    '#default_value' => variable_get('shurlyauto_node_type', ''),
  );
  
  return system_settings_form($form);
}
